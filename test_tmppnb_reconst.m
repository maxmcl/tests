%% Testing MinConf_TMP to solve the projection sub-problem
clc;
clear all;
% close all;

%% Initializing the directories
global ROOTDIR
% Defining the root directory
ROOTDIR = fullfile(getenv('HOME'), 'Masters');
% setPaths is in recUtils
addpath(fullfile(ROOTDIR, 'recUtils'));
% Adding all the other repositories to the path
setPaths; % edit this function accordingly
% Folder where the phantom is saved
DATADIR = fullfile(ROOTDIR, 'TestData');
% Folder where the projection matrices are saved
MPDIR = fullfile(DATADIR, 'projMatrices');

addpath(fullfile(ROOTDIR, 'reconst-tests'));

%% Creating the phantom data and the sinogram
% --- genData parameters ---
% Resolution reduction factor compared to original (>= 1)
FACTOR = 4;
% --- genCrit parameters ---
critPars.lambda = 1e-00;
critPars.delta = 5e-03;
critPars.penalType = 'PenalGradObj_L2';
critPars.coordType = 'cyl';
critPars.imagPenal = false;
% Chosing set of parameters to use for the phantom
[fant, sino] = genData(DATADIR, FACTOR, ['phant_', num2str(FACTOR)]);

%% Creating the cartesian coordinates objective function object (Critere)
[crit, critPars] = genCrit(fant, sino, DATADIR, MPDIR, critPars);

%% Build the preconditionner
prec = Precond_Factory.create('DiagF', 'crit', crit);

%% Solvers parameters
TOL = 1e-10;
PROJ_TOL = 1e-12;
MAX_ITER = 1e3;
PROJ_MAX_ITER = 5e4;
MAX_PROJ = 5e4;
MAX_EVAL = 1e3;
PROJ_MAX_EVAL = 5e4;
VERBOSE = 2;
PROJ_VERBOSE = 0;
MAX_RT = 15 * 60;
PROJ_MAX_RT = 5 * 60;

%% Solving the reconstruction problem
import model.ProjRecModel;
import model.ProjModel;

reconstNames = {'Bb'};
projNames = {'Bcflash', 'Bcflash2'};

recDefault = struct('aOptTol', TOL, 'maxIter', MAX_ITER, ...
    'maxEval', MAX_EVAL, 'maxProj', MAX_PROJ, 'verbose', VERBOSE, ...
    'maxRT', MAX_RT);
projDefault = struct('aOptTol', PROJ_TOL, 'maxIter', PROJ_MAX_ITER, ...
    'maxEval', PROJ_MAX_EVAL, 'verbose', PROJ_VERBOSE, ...
    'maxRT', PROJ_MAX_RT, 'useBb', true, 'backtracking', true);

% Initialize every solvers options to default and then add custom params
recOpts.Bb = recDefault;
recOpts.bbType = 'BB1';
projOpts.Bcflash2 = projDefault;
projOpts.Bcflash = projDefault;

% Test Tmp for times using different solvers
methods = {'pcg', 'minres', 'lsmr', 'lsqr', ...
    'pcg', 'minres', 'lsmr', 'lsqr'};

methods = {'lsqr'};

% Save the stats structs inside this struct
% Keep track of factor and general optimization parameters
data.FACTOR = FACTOR;
data.recOpts = recDefault;
data.projOpts = projDefault;

for recName = reconstNames(1)
    
    % Current reconstruction solver's name
    recSolverType = ['solvers.', recName{1}, 'Solver'];
    tmpCounter = 1;
    
    for projName = projNames
        
        if strcmp(projName{1}, 'Tmp2') || strcmp(projName{1}, 'Pnb')
            projOpts.Tmp.method = methods{tmpCounter};
            projOpts.Tmp2.method = methods{tmpCounter};
            tmpCounter = tmpCounter + 1;
        end
        
        % Current projection solver's name
        projSolverType = ['solvers.', projName{1}, 'Solver'];
        
        % Building the projection model
        projModel = ProjModel(prec, crit.J{2}.GeoS);
        % Creating the projection solver
        projSolver = eval([projSolverType, ...
            '(projModel, projOpts.(projName{1}))']);
        
        % Building the reconstruction model
        recModel = ProjRecModel(crit, prec, sino, crit.J{2}.GeoS, ...
            projSolver);
        
        % Creating the solver
        recSolver = eval([recSolverType, ...
            '(recModel, recOpts.(recName{1}))']);
        
        % Solving the problem
        recSolver.solve();
        
        % Plotting the figure
        if strcmp(projName{1}, 'Tmp2') || strcmp(projName{1}, 'Pnb')
            figName = [recName{1}, '_', projName{1}, '_', ...
                projOpts.Tmp.method];
            pName = [projName{1}, projOpts.Tmp.method];
        else
            figName = [recName{1}, '_', projName{1}];
            pName = projName{1};
        end
        
        srie = genSerie(recSolver.x, crit.J{2}.GeoS, sino, prec, ...
            true, figName);
        
        %% Save run statistics
        data.(recName{1}).(pName) = recSolver.stats;
        data.(recName{1}).(pName).x = srie.Val(:);
    end
    
end

%% Saving data struct
[~, filename] = fetchNames(FACTOR, 'bcflash', './runData/');
save(filename, 'data');

plotStruct(data);