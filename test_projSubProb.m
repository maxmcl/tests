%% Testing MinConf_TMP to solve the projection sub-problem
clc;
clear all;
close all;

%% Initializing the directories
global ROOTDIR
% Defining the root directory
ROOTDIR = fullfile(getenv('HOME'), 'Masters');
% setPaths is in recUtils
addpath(fullfile(ROOTDIR, 'recUtils'));
% Adding all the other repositories to the path
setPaths; % edit this function accordingly
% Folder where the phantom is saved
DATADIR = fullfile(ROOTDIR, 'TestData');
% Folder where the projection matrices are saved
MPDIR = fullfile(DATADIR, 'projMatrices');

%% Creating the phantom data and the sinogram
% --- genData parameters ---
% Resolution reduction factor compared to original (>= 1)
FACTOR = 4;
% --- genCrit parameters ---
critPars.lambda = 1e-00;
critPars.delta = 5e-03;
critPars.penalType = 'PenalGradObj_L2';
critPars.coordType = 'cyl';
critPars.imagPenal = false;
% Chosing set of parameters to use for the phantom
[fant, sino] = genData(DATADIR, FACTOR, ['phant_', num2str(FACTOR)]);

%% Creating the cartesian coordinates objective function object (Critere)
[crit, critPars] = genCrit(fant, sino, DATADIR, MPDIR, critPars);

%% Build the preconditionner
prec = Precond_Factory.create('DiagF', 'crit', crit);

%% Format
HEADER = {'Solver', 'Range', '#iter', '#f', '#g', '#H', '|Pg|', 'RT'};
HEADER_LATEX = {'Solver & ', 'Range & ', '#iter & ', '\#f & ', ...
    '\#g & ', '\#H & ', '|Pg| & ', 'RT \\'};
HEADER_FORMAT = ['%25s', repmat('%10s', 1, 7), '\n'];
BODY_FORMAT = '%25s %10d %10d %10d %10d %10d %10.1e %10.1e\n';
BODY_LATEX = ['%25s %3s %10d %3s %10d %3s %10d %3s %10d %3s %10d %3s', ...
    ' %10.1e %3s %10.1e %5s \n'];

%% Solve
TOL = 1e-10;
MAX_ITER = 1e4;
MAX_EVAL = 1e4;
FUNC_TOL = eps;

import model.ProjModel;

import solvers.Tmp2Solver;
mcOpts.aOptTol = TOL;
mcOpts.maxIter = MAX_ITER;
mcOpts.maxEval = MAX_EVAL;
mcOpts.aFeasTol = FUNC_TOL;
mcOpts.verbose = 1;

projModel = ProjModel(prec, crit.J{2}.GeoS);

RANGE = 50;
x0 = 10*ones(projModel.objSize, 1);
range = round(RANGE/100 * projModel.objSize);
x0(1 : range) = -10*ones(range, 1); % Fill with -10 according to RANGE

dataD = struct;
dataRnorm = struct;

%% TMP
% LSQR (Spot)
projModel.setPointToProject(-x0);
mcOpts.method = 'lsqr';
solver = Tmp2Solver(projModel, mcOpts);
solver.solve();

dataD.LSQR = solver.saveD;
dataRnorm.LSQR = solver.saveRnorm;

% LSMR (Spot)
projModel = ProjModel(prec, crit.J{2}.GeoS);
projModel.setPointToProject(-x0);
mcOpts.method = 'lsmr';
solver = Tmp2Solver(projModel, mcOpts);
solver.solve();

dataD.LSMR = solver.saveD;
dataRnorm.LSMR = solver.saveRnorm;

% PCG
projModel = ProjModel(prec, crit.J{2}.GeoS);
projModel.setPointToProject(-x0);
mcOpts.method = 'pcg';
solver = Tmp2Solver(projModel, mcOpts);
solver.solve();

dataD.PCG = solver.saveD;
dataRnorm.PCG = solver.saveRnorm;

% MinRes
projModel = ProjModel(prec, crit.J{2}.GeoS);
projModel.setPointToProject(-x0);
mcOpts.method = 'minres';
solver = Tmp2Solver(projModel, mcOpts);
solver.solve();

dataD.MinRes = solver.saveD;
dataRnorm.MinRes = solver.saveRnorm;

structName = sprintf('runData/checkKrylov-%s.mat', num2str(RANGE));
save(structName, 'dataD', 'dataRnorm');

%% Plot solution found vs proj algorithm iter
m = min([length(dataD.LSQR), length(dataD.LSMR), ...
    length(dataD.PCG), length(dataD.MinRes)]);
pcgLsqr = zeros(m, 1);
pcgLsmr = zeros(m, 1);
pcgMinres = zeros(m, 1);
for i = 1 : m
    tempPcg = dataD.PCG{i};
    
    pcgLsqr(i) = norm(tempPcg - dataD.LSQR{i});
    pcgLsmr(i) = norm(tempPcg - dataD.LSMR{i});
    pcgMinres(i) = norm(tempPcg - dataD.MinRes{i});
end

h = figure;
semilogy(pcgLsqr, 'k-d');
hold on;
semilogy(pcgLsmr, 'c');
semilogy(pcgMinres, 'y');
hold off;
legend({'||d_{PCG} - d_{LSQR}||', '||d_{PCG} - d_{LSMR}||', ...
    '||d_{PCG} - d_{MinRes}||'}, 'location', 'best');
xlabel('proj algo iter');
ylabel('\Delta');

figName = sprintf('runData/d-iter-%s', num2str(RANGE));
print(h, '-depsc', '-r200', figName);

%% Residual vector vs Krylov solver's iter

h = figure;
for i = 1 : m
    ylabel(sprintf('rNorm (outer iter = %d)', i));
    semilogy(dataRnorm.PCG{i}, 'r');
    hold on;
    semilogy(dataRnorm.LSQR{i}, 'b');
    semilogy(dataRnorm.MinRes{i}, 'g');
    semilogy(dataRnorm.LSMR{i}, 'm');
    pause;
end
hold off;
legend({'rNorm_{PCG}', 'rNorm_{LSQR}', 'rNorm_{MinRes}', ...
    'rNorm_{LSMR}'}, 'location', 'best');
xlabel('inner iter');

%% Residual value upon exit vs projection algorithm iteration

% Collecting
tempPcg = zeros(m, 1);
tempLsqr = zeros(m, 1);
tempMinRes = zeros(m , 1);
tempLsmr = zeros(m , 1);
for i = 1 : m
    tempPcg(i) = dataRnorm.PCG{i}(end);
    tempLsqr(i) = dataRnorm.LSQR{i}(end);
    tempMinRes(i) = dataRnorm.MinRes{i}(end);
    tempLsmr(i) = dataRnorm.LSMR{i}(end);
end

h = figure;
semilogy(tempPcg, 'r');
hold on;
semilogy(tempLsqr, 'b-d');
semilogy(tempMinRes, 'g');
semilogy(tempLsmr, 'm');
hold off;
legend({'rNorm_{PCG}', 'rNorm_{LSQR}', 'rNorm_{MinRes}', ...
    'rNorm_{LSMR}'}, 'location', 'best');
xlabel('proj algo iter');
ylabel('rNorm on exit');

figName = sprintf('runData/rNorm-iter-%s', num2str(RANGE));
print(h, '-depsc', '-r200', figName);

%% To be removed later
h = figure;
semilogy(tempPcg, 'r');
hold on;
semilogy(tempMinRes, 'g');
hold off;
legend({'rNorm_{PCG}', 'rNorm_{MinRes}'}, 'location', 'best');
xlabel('proj algo iter');
ylabel('rNorm on exit');
figName = sprintf('runData/rNorm-iter-pcg-%s', num2str(RANGE));
print(h, '-depsc', '-r200', figName);

h = figure;
semilogy(tempLsqr, 'b-d');
hold on;
semilogy(tempLsmr, 'm');
hold off;
legend({'rNorm_{LSQR}', 'rNorm_{LSMR}'}, 'location', 'best');
xlabel('proj algo iter');
ylabel('rNorm on exit');
figName = sprintf('runData/rNorm-iter-lsqr-%s', num2str(RANGE));
print(h, '-depsc', '-r200', figName);