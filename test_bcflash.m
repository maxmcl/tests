%% Testing MinConf_TMP to solve the projection sub-problem
clc;
clear all;
close all;

%% Initializing the directories
global ROOTDIR
% Defining the root directory
ROOTDIR = fullfile(getenv('HOME'), 'Masters');
% setPaths is in recUtils
addpath(fullfile(ROOTDIR, 'recUtils'));
% Adding all the other repositories to the path
setPaths; % edit this function accordingly
% Folder where the phantom is saved
DATADIR = fullfile(ROOTDIR, 'TestData');
% Folder where the projection matrices are saved
MPDIR = fullfile(DATADIR, 'projMatrices');

addpath(fullfile(ROOTDIR, 'reconst-tests'));

%% Creating the phantom data and the sinogram
% --- genData parameters ---
% Resolution reduction factor compared to original (>= 1)
FACTOR = 1;
% --- genCrit parameters ---
critPars.lambda = 1e-00;
critPars.delta = 5e-03;
critPars.penalType = 'PenalGradObj_L2';
critPars.coordType = 'cyl';
critPars.imagPenal = false;
% Chosing set of parameters to use for the phantom
[fant, sino] = genData(DATADIR, FACTOR, ['phant_', num2str(FACTOR)]);

%% Creating the cartesian coordinates objective function object (Critere)
[crit, critPars] = genCrit(fant, sino, DATADIR, MPDIR, critPars);

%% Build the preconditionner
prec = Precond_Factory.create('Identite', 'crit', crit);

%% Solvers parameters
TOL = 1e-10;
A_FEAS_TOL = 1e-15;
MAX_ITER = 1e3;
MAX_EVAL = 1e3;
VERBOSE = 2;
MAX_RT = 15 * 60;

%% Solving the reconstruction problem
import model.RecModel;  % rec
import solvers.BcflashSolver;

recOpts = struct('aOptTol', TOL, 'aFeasTol', A_FEAS_TOL, 'maxIter', ...
    MAX_ITER, 'maxEval', MAX_EVAL, 'verbose', VERBOSE, 'maxRT', MAX_RT);
recOpts.useBb = true;
recOpts.backtracking = true;

% Save the stats structs inside this struct
% Keep track of factor and general optimization parameters
data.FACTOR = FACTOR;
data.recOpts = recOpts;

% Building the reconstruction model
recModel = RecModel(crit, prec, sino, crit.J{2}.GeoS, [], '');

% Creating the solver
recSolver = BcflashSolver(recModel, recOpts);

% Solving the problem
recSolver.solve();

srie = genSerie(recSolver.x, crit.J{2}.GeoS, sino, prec, ...
    true, '');