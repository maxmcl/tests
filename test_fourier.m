clc;
clear all;
close all;

%% Adding Spot library to path
addpath('~/Masters/Spot');

%% Building CCt
mm = 3;
nn = mm;

blkSize = 3;
nBlks = 4;

a1 = 10 * eye(mm);
a2 = [1, 2, 0; 2, 1, 3; 0, 3, 1];
a3 = ones(mm);
% Block-circulant matrix (positive eigenvalues)
A = [a1, a2, a3, a2; a2, a1, a2, a3; a3, a2, a1, a2; a2, a3, a2, a1];
[m, n] = size(A);
% Building a DFT matrix
opF = opDFT(m);
% Using the spectral decomposition theorem
Pi = round(opF * A * opF', 12);
% Extracting the diagonal from the block-diagonal matrix
D = diag(Pi);
% Creating CCt - a ciruclant matrix
CCt = opF' * (opDiag(D.^2) * opF);
clear a1 a2 A Pi;
x = (1 : m)';
% Creating a restriction on CCt or C
index = false(m, 1);
index([3:5:end]) = true;
ind = index;
B = opRestriction(m, ind);

%% Block FFTs (by rearranging and row-FFTs)
Cx2 = reshape(ifft(reshape(D, blkSize, nBlks) .* ...
    fft(reshape(x, blkSize, nBlks), [], 2), [], 2), [], 1);

% Correct expression of C
C2 = reshape(ifft(repmat(reshape(D, blkSize, nBlks), 1, m) .* ...
    fft(reshape(eye(m), blkSize, []), [], 2), [], 2), m, m);

Cx22 = C2 * x; % ok

dC2 = diag(C2);
dC2(ind)
% OK
% dC2(index) .* x(index)
% C2(index, index) * x(index)

%%
% Converting logical to positions
ind = find(index);

% Finding (block size, # blocks) equivalent of ind positions
[r, c] = ind2sub([blkSize, nBlks], ind);

% Parsing only once for each block
[r, iC, iR] = unique(r);
nR = length(r);

% Retrieving positions in reduced matrix
redInd = sub2ind([nR, nBlks], 1:nR, c(iC)');
% Initializing rows of identity matrix matching ind
p = zeros(nR, nBlks);
p(redInd) = 1;

dC = zeros(nR, 1);
for ii = 1 : nR
    temp = ifft(D(r(ii) : blkSize : end)' .* fft(p(ii, :), [], 2), [], 2);
    dC(ii) = temp(c(iC(ii)));
end
dC = dC(iR);
dC

%%
% Converting logical to positions
ind = find(index);

% Finding (block size, # blocks) equivalent of ind positions
[r, c] = ind2sub([blkSize, nBlks], ind);

% Parsing only once for each block
[r, iC, iR] = unique(r);
nR = length(r);

dC = zeros(nR, 1);
for ii = 1 : nR
    p = zeros(1, nBlks);
    p(c(ii)) = 1;
    temp = ifft(D(r(ii) : blkSize : end)' .* fft(p, [], 2), [], 2);
    dC(ii) = temp(c(ii));
end
dC = dC(iR);
dC

%%

Z = zeros(blkSize, nBlks);
Z(1:blkSize, 1:blkSize) = eye(blkSize);
diagC = diag(ifft(reshape(D, blkSize, nBlks) .* ...
    fft(Z, [], 2), [], 2));


