clc;
clear all;
close all;

%% Initializing the directories
global ROOTDIR
% Defining the root directory
ROOTDIR = fullfile(getenv('HOME'), 'Masters');
% setPaths is in recUtils
addpath(fullfile(ROOTDIR, 'recUtils'));
% Adding all the other repositories to the path
setPaths; % edit this function accordingly
% Folder where the phantom is saved
DATADIR = fullfile(ROOTDIR, 'TestData');
% Folder where the projection matrices are saved
MPDIR = fullfile(DATADIR, 'projMatrices');

%%
load('linSys_Tmp2.mat');
clear nlp;

d2 = d;

temp = -self.nlp.getResidual(x);
import krylov.lsqr_spot;
% fprintf('iter = %5d  |  cs1 = %5.2f  | sn = %5.2f\n', itn, cs1, sn); 
[temp2, ~, stats] = krylov.lsqr_spot(self.nlp.A(:, working), temp, ...
    self.krylOpts);
d(working) = temp2;

d2(working) = self.descDirFunc(self, x, g, H, working);

figure;
plot(-g(working), 'ro');
hold on;
g2 = real(self.nlp.prec.Direct(-temp));
plot(-g2(working), 'b');
hold off;