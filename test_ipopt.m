%% IPOPT test file using AMPL problems
clc;
clear all;
close all;

%% Initializing the directories
global ROOTDIR
% Defining the root directory
ROOTDIR = fullfile(getenv('HOME'), 'Masters');
% setPaths is in recUtils
addpath(fullfile(ROOTDIR, 'recUtils'));
% Adding all the other repositories to the path
setPaths; % edit this function accordingly

%% Getting the problems
% Folder that might contain the .nl problem files
lookInto = '~/Masters/decoded_ampl_models';
% File containing the problems name
problemsFile = './bounds-new.lst';
fold = './ipopt-vs-bcflash';

import utils.findProblems;
[problems, notFound] = utils.findProblems(lookInto, problemsFile);

%% Solve the problems
import solvers.IpoptSolver;
import solvers.BcflashSolver;
import model.AmplModel;

% Set up the solvers' general parameters
solverOpts = struct('aOptTol', 1e-10, 'aFeasTol', 1e-30, ...
    'maxIter', 1e4, 'verbose', 2, 'maxRT', 2*60, 'maxEval', 1e4, ...
    'aCompTol', 1e-10);
ipoptOpts = solverOpts;

% Save everything in 'data' struct
data = struct;
data.infoHeader = {'pgNorm', 'solveTime', 'fx', 'iter', 'nObjFunc', ...
    'nGrad', 'nHess'};
data.solverNames = {'Ipopt', 'Bcflash'};
data.Ipopt = {};
data.Bcflash = {};
data.normX = [];
data.failed = {}; % Keep track of failures
% % Store performance profile data in a 3D matrix
nm = length(data.infoHeader);
data.pMat = nan(length(problems), length(data.solverNames), nm);

nProb = 1;
for problem = problems
    % For each problem
    fprintf('\n\n--- %s ---\n\n', problem{1});
    % For each solver
    try
        
        % Load the problem, sparse = true
        nlp = model.AmplModel(problem{1}, true);
        % Calling the solver
        bcflashSolver = solvers.BcflashSolver(nlp, solverOpts);
        bcflashSolver.solve();
        
        % Load the problem, sparse = true
        temp = max(bcflashSolver.rOptTol, eps);
        ipoptOpts.aOptTol = temp;
        ipopOpts.aCompTol = temp;
        nlp = model.AmplModel(problem{1}, true);
        % Calling the solver
        ipoptSolver = solvers.IpoptSolver(nlp, ipoptOpts);
        ipoptSolver.solve();
        
        % Updating the performance profile data matrix
        tempIpopt = [ipoptSolver.pgNorm, ipoptSolver.solveTime, ...
            ipoptSolver.fx, ipoptSolver.iter, ipoptSolver.nObjFunc, ...
            ipoptSolver.nGrad, ipoptSolver.nHess];
        tempBcflash = [bcflashSolver.pgNorm, bcflashSolver.solveTime, ...
            bcflashSolver.fx, bcflashSolver.iter, ...
            bcflashSolver.nObjFunc, bcflashSolver.nGrad, ...
            bcflashSolver.nHess];
        
    catch ME
        % Some problems return function evaluation failures
        warning('%s\n', ME.message);
        data.failed{end + 1} = problem{1};
        continue;
    end
    
    data.pMat(nProb, 1, 1 : nm) = tempIpopt;
    data.pMat(nProb, 2, 1 : nm) = tempBcflash;
    % Storing other stuff
    data.Ipopt{end + 1} = {ipoptSolver.x, ...
        ipoptSolver.EXIT_MSG{ipoptSolver.iStop}};
    data.Bcflash{end + 1} = {bcflashSolver.x, ...
        bcflashSolver.EXIT_MSG{bcflashSolver.iStop}};
    data.normX = [data.normX; norm(ipoptSolver.x - bcflashSolver.x) / ...
        norm(ipoptSolver.x)];
    
    nProb = nProb + 1;
end

[~, pname, ~] = fileparts(problemsFile);
save([fold, '/', pname, '-ipopt-bcflash'], 'data');

%% Compare on x & fx

% ||\Delta x^*||
figName = [fold, '/normx.eps'];
h = figure('DefaultAxesFontSize', 14);
temp = data.normX;
temp(isnan(temp)) = min(temp);
temp = log10(temp);
hist(temp);
ylabel('# of problems');
xlabel('log10(||x^*_{Bcflash} - x^*_{Ipopt}|| / ||x^*_{Ipopt}||)');
legend('Bcflash vs Ipopt', 'location', 'northwest');
[~, pp] = h.Children.Children;
pp(1).FaceColor = [0, 0, 1];
% Save figure
print(h, '-depsc', '-r200', figName);

% | \Delta fx |
figName = [fold, '/fx.eps'];
h = figure('DefaultAxesFontSize', 14);
temp = abs(data.pMat(:, 1, 3) - data.pMat(:, 2, 3)) ./ ...
    abs(data.pMat(:, 1, 3));
temp(isnan(temp)) = min(temp);
temp = log10(temp);
hist(temp);
ylabel('# of problems');
xlabel('log10(|fx*_{Bcflash} - fx*_{Ipopt}| / |fx*_{Ipopt}|)');
legend('Bcflash vs Ipopt', 'location', 'northwest');
[~, pp] = h.Children.Children;
pp(1).FaceColor = [1, 0, 0];
% Save figure
print(h, '-depsc', '-r200', figName);