
EXIT TMP: Directional derivative below aOptTol\n
CONVERGENCE: 1
||Pg|| =  2.0e-06
Stop tolerance =  5.7e-08

EXIT TMP: Directional derivative below aOptTol\n
CONVERGENCE: 1
||Pg|| =  2.1e-06
Stop tolerance =  5.7e-08

EXIT TMP: All working variables satisfy optimality condition\n
CONVERGENCE: 1
||Pg|| =  1.3e-08
Stop tolerance =  5.7e-08

EXIT TMP: All working variables satisfy optimality condition\n
CONVERGENCE: 1
||Pg|| =  2.6e-09
Stop tolerance =  5.7e-08

EXIT PNB: All working variables satisfy optimality condition\n
CONVERGENCE: 1
||Pg|| =  1.5e-08
Stop tolerance =  5.7e-08

EXIT Bb: Function value changing by less than frTol\n
CONVERGENCE: 1
||Pg|| =  2.3e-05
Stop tolerance =  5.7e-08

EXIT SPG: Function value changing by less than feasTol\n
CONVERGENCE: 1
||Pg|| =  4.9e-05
Stop tolerance =  5.7e-08

EXIT PQN: First-Order Optimality Conditions Below aOptTol\n
CONVERGENCE: 1
||Pg|| =  4.8e-08
Stop tolerance =  5.7e-08
[Warning: Projection sub-problem didn't converge: NCONV - Null relative descent stops the solve; have we converged, though?] 
[> In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('solvers.LbfgsbSolver/solve', '/home/max/Masters/nlplab/+solvers/LbfgsbSolver.m', 73)" style="font-weight:bold">solvers.LbfgsbSolver/solve</a> (<a href="matlab: opentoline('/home/max/Masters/nlplab/+solvers/LbfgsbSolver.m',73,0)">line 73</a>)
  In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('test_projSubProb', '/home/max/Masters/tests/test_projSubProb.m', 320)" style="font-weight:bold">test_projSubProb</a> (<a href="matlab: opentoline('/home/max/Masters/tests/test_projSubProb.m',320,0)">line 320</a>)] 

EXIT L-BFGS-B: NCONV - Null relative descent stops the solve; have we converged, though?
||Pg|| =  9.2e-07

EXIT bcflash: Optimal solution found
CONVERGENCE: 1
||Pg|| =  4.7e-08
Stop tolerance =  5.7e-08
