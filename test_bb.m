%% Testing MinConf_TMP to solve the projection sub-problem
% clc;
clear all;
close all;

%% Initializing the directories
global ROOTDIR
% Defining the root directory
ROOTDIR = fullfile(getenv('HOME'), 'Masters');
% setPaths is in recUtils
addpath(fullfile(ROOTDIR, 'recUtils'));
% Adding all the other repositories to the path
setPaths; % edit this function accordingly
% Folder where the phantom is saved
DATADIR = fullfile(ROOTDIR, 'TestData');
% Folder where the projection matrices are saved
MPDIR = fullfile(DATADIR, 'projMatrices');

addpath(fullfile(ROOTDIR, 'reconst-tests'));

%% Creating the phantom data and the sinogram
FACTOR = 1;
critPars.lambda = 0.5;
critPars.delta = 5e-03;
critPars.penalType = 'PenalGradObj_L2';
critPars.coordType = 'cyl';
critPars.imagPenal = false;

[fant, sino] = genData(DATADIR, FACTOR, ['phant_', num2str(FACTOR)]);
[crit, critPars] = genCrit(fant, sino, DATADIR, MPDIR, critPars);
prec = Precond_Factory.create('DiagF', 'crit', crit);

%% Solvers parameters
TOL = 1e-10;
A_FEAS_TOL = eps;
PROJ_TOL = 1e-12;
MAX_ITER = 1e3;
PROJ_MAX_ITER = 2e4;
MAX_PROJ = 2e4;
MAX_EVAL = 1e3;
PROJ_MAX_EVAL = 2e4;
VERBOSE = 1;
PROJ_VERBOSE = 0;
MAX_RT = 15 * 60;
PROJ_MAX_RT = 5 * 60;

%% Solving the reconstruction problem
import model.ProjRecModel;  % rec
import model.ProjModel;     % proj
import solvers.BbSolver;
import solvers.Tmp2Solver;

recOpts = struct('aOptTol', TOL, 'maxIter', MAX_ITER, ...
    'maxEval', MAX_EVAL, 'maxProj', MAX_PROJ, 'verbose', VERBOSE, ...
    'maxRT', MAX_RT, 'aFeasTol', A_FEAS_TOL);
projOpts = struct('aOptTol', PROJ_TOL, 'maxIter', PROJ_MAX_ITER, ...
    'maxEval', PROJ_MAX_EVAL, 'verbose', PROJ_VERBOSE, ...
    'maxRT', PROJ_MAX_RT, 'aFeasTol', A_FEAS_TOL);

%% Save run statistics in this struct
data = struct;
data.FACTOR = FACTOR;
data.recOpts = recOpts;
data.projOpts = projOpts;
data.solvers = 'BbSolver and Tmp2Solver';
data.infoHeader = {'memory', 'pgNorm', 'solveTime', 'fx', 'nrmViol', ...
    'nObjFunc', 'nGrad', 'nHess', 'iter', 'nProj', 'iStop'};
data.bbTypes = {'bb1', 'abbmin1', 'abbss'};
data.memoryVals = 1 : 1 : 10;
mL = length(data.memoryVals);
mP = length(data.bbTypes);
mM = length(data.infoHeader);
data.pMat = nan(mL, mP, mM);

[~, filename] = fetchNames(FACTOR, 'bb-study-memory', './bb-study/');

%% Solving everything
% Calling the models
import model.ProjModel;
import model.ProjRecModel;
import solvers.BbSolver;
import solvers.Tmp2Solver;

% For reconstruction solvers
nL = 1;
for memoryVal = data.memoryVals
    nP = 1;
    for bbType = data.bbTypes
        
        fprintf('\n\n --- memory = %d, bbType = %s --- \n\n', ...
            memoryVal, bbType{1});
        
        % Building the projection model & solver
        projModel = ProjModel(prec, crit.J{2}.GeoS);
        projSolver = solvers.Tmp2Solver(projModel, projOpts);
        
        % Building the reconstruction model & solver
        recModel = ProjRecModel(crit, prec, sino, crit.J{2}.GeoS, ...
            projSolver);
        recOpts.bbType = bbType{1};
        recOpts.memory = memoryVal;
        recSolver = solvers.BbSolver(recModel, recOpts);
        % Solving the problem
        recSolver.solve();
        
        %% Save run statistics
        srie = genSerie(recSolver.x, crit.J{2}.GeoS, sino, ...
            prec, false, '');
        nrmViol = norm(srie.Val(srie.Val < 0));
        stats = [memoryVal, recSolver.pgNorm, recSolver.solveTime, ...
            recSolver.fx, nrmViol, recSolver.nObjFunc, recSolver.nGrad, ...
            recSolver.nHess, recSolver.iter, recSolver.nProj, ...
            recSolver.iStop];
        data.pMat(nL, nP, :) = stats;
        
        % Saving data struct -- preemptively in case MATLAB crashes
        save(filename, 'data');
        
        nP = nP + 1;
    end
    nL = nL + 1;
end