%% Testing MinConf_TMP to solve the projection sub-problem
clc;
clear all;
close all;

%% Initializing the directories
global ROOTDIR
% Defining the root directory
ROOTDIR = fullfile(getenv('HOME'), 'Masters');
% setPaths is in recUtils
addpath(fullfile(ROOTDIR, 'recUtils'));
% Adding all the other repositories to the path
setPaths; % edit this function accordingly
% Folder where the phantom is saved
DATADIR = fullfile(ROOTDIR, 'TestData');
% Folder where the projection matrices are saved
MPDIR = fullfile(DATADIR, 'projMatrices');

addpath(fullfile(ROOTDIR, 'reconst-tests'));

%% Creating the phantom data and the sinogram
% --- genData parameters ---
% Resolution reduction factor compared to original (>= 1)
FACTOR = 4;
% --- genCrit parameters ---
critPars.lambda = 1e-00;
critPars.delta = 5e-03;
critPars.penalType = 'PenalGradObj_L2';
critPars.coordType = 'cyl';
critPars.imagPenal = false;
% Chosing set of parameters to use for the phantom
[fant, sino] = genData(DATADIR, FACTOR, ['phant_', num2str(FACTOR)]);

%% Creating the cartesian coordinates objective function object (Critere)
[crit, critPars] = genCrit(fant, sino, DATADIR, MPDIR, critPars);

%% Build the preconditionner
prec = Precond_Factory.create('DiagF', 'crit', crit);

%% Solvers parameters
TOL = 1e-6;
PROJ_TOL = 1e-8;
MAX_ITER = 1e5;
PROJ_MAX_ITER = 1e5;
MAX_PROJ = 1e5;
MAX_EVAL = 1e5;
PROJ_MAX_EVAL = 1e5;
VERBOSE = 2;
PROJ_VERBOSE = 0;
MAX_RT = 15 * 60;
PROJ_MAX_RT = 5 * 60;

%% Solving the reconstruction problem
import model.ProjRecModel;  % rec
import model.ProjModel;     % proj
import solvers.BbSolver;    % rec
import solvers.PnbSolver;   % proj
import solvers.TmpSolver;   % proj

recOpts = struct('aOptTol', TOL, 'maxIter', MAX_ITER, ...
    'maxEval', MAX_EVAL, 'maxProj', MAX_PROJ, 'verbose', VERBOSE, ...
    'maxRT', MAX_RT);
projOpts = struct('aOptTol', PROJ_TOL, 'maxIter', PROJ_MAX_ITER, ...
    'maxEval', PROJ_MAX_EVAL, 'verbose', PROJ_VERBOSE, ...
    'maxRT', PROJ_MAX_RT);

% Save the stats structs inside this struct
% Keep track of factor and general optimization parameters
data.FACTOR = FACTOR;
data.recOpts = recOpts;
data.projOpts = projOpts;

%% Pnb
for temp = {'noPrec', 'precBCCB', 'precD'}
    precond = temp{1};
    projOpts.precond = precond;
    
    % Building the projection model
    projModel = ProjModel(prec, crit.J{2}.GeoS);
    % Creating the projection solver
    projSolver = PnbSolver(projModel, projOpts);
    
    % Building the reconstruction model
    recModel = ProjRecModel(crit, prec, sino, crit.J{2}.GeoS, ...
        projSolver);
    
    % Creating the solver
    recSolver = BbSolver(recModel, recOpts);
    
    % Solving the problem
    recSolver.solve();
    
    % Plotting the figure
    if strcmp(precond, 'noPrec')
        figName = 'BbPnbNoPrec';
    elseif strcmp(precond, 'precBCCB')
        figName = 'BbPnbPrecBCCB';
    else
        figName = 'BbPnbPrecD';
    end
    
    srie = genSerie(recSolver.x, crit.J{2}.GeoS, sino, prec, ...
        true, figName);
    
    data.Bb.(['Pnb', precond]) = recSolver.stats;
    data.Bb.(['Pnb', precond]).x = srie.Val(:);
    
end

%% Tmp
for temp = {'noPrec', 'precBCCB', 'precD'}
    precond = temp{1};
    projOpts.precond = precond;
    
    % Building the projection model
    projModel = ProjModel(prec, crit.J{2}.GeoS);
    % Creating the projection solver
    projSolver = TmpSolver(projModel, projOpts);
    
    % Building the reconstruction model
    recModel = ProjRecModel(crit, prec, sino, crit.J{2}.GeoS, ...
        projSolver);
    
    % Creating the solver
    recSolver = BbSolver(recModel, recOpts);
    
    % Solving the problem
    recSolver.solve();
    
    % Plotting the figure
    if strcmp(precond, 'noPrec')
        figName = 'BbTmpNoPrec';
    elseif strcmp(precond, 'precBCCB')
        figName = 'BbTmpPrecBCCB';
    else
        figName = 'BbTmpPrecD';
    end
    
    srie = genSerie(recSolver.x, crit.J{2}.GeoS, sino, prec, ...
        true, figName);
    
    data.Bb.(['Tmp', precond]) = recSolver.stats;
    data.Bb.(['Tmp', precond]).x = srie.Val(:);
    
end

%% Saving data struct
[~, filename] = fetchNames(FACTOR, 'rec_pnb_precond', './runData/');
save(filename, 'data');

plotStruct(data);