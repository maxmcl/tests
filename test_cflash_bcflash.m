%% Testing MinConf_TMP to solve the projection sub-problem
% clc;
clear all;
close all;

%% Initializing the directories
global ROOTDIR
% Defining the root directory
ROOTDIR = fullfile(getenv('HOME'), 'Masters');
% setPaths is in recUtils
addpath(fullfile(ROOTDIR, 'recUtils'));
% Adding all the other repositories to the path
setPaths; % edit this function accordingly
% Folder where the phantom is saved
DATADIR = fullfile(ROOTDIR, 'TestData');
% Folder where the projection matrices are saved
MPDIR = fullfile(DATADIR, 'projMatrices');

addpath(fullfile(ROOTDIR, 'reconst-tests'));

%% Creating the phantom data and the sinogram
FACTOR = 1;
critPars.lambda = 0.5;
critPars.delta = 5e-03;
critPars.penalType = 'PenalGradObj_L2';
critPars.coordType = 'cyl';
critPars.imagPenal = false;

[fant, sino] = genData(DATADIR, FACTOR, ['phant_', num2str(FACTOR)]);
[crit, critPars] = genCrit(fant, sino, DATADIR, MPDIR, critPars);
prec = Precond_Factory.create('DiagF', 'crit', crit);

%% Solvers parameters
TOL = 1e-10;
A_FEAS_TOL = eps;
PROJ_TOL = 1e-12;
MAX_ITER = 1e3;
PROJ_MAX_ITER = 2e4;
MAX_PROJ = 2e4;
MAX_EVAL = 1e3;
PROJ_MAX_EVAL = 2e4;
VERBOSE = 1;
PROJ_VERBOSE = 0;
MAX_RT = 15 * 60;
PROJ_MAX_RT = 5 * 60;

recOpts = struct('aOptTol', TOL, 'maxIter', MAX_ITER, ...
    'maxEval', MAX_EVAL, 'maxProj', MAX_PROJ, 'verbose', VERBOSE, ...
    'maxRT', MAX_RT, 'aFeasTol', A_FEAS_TOL);
recOpts.backtracking = true;
recOpts.cgTol = 1e-2;
projOpts = struct('aOptTol', PROJ_TOL, 'maxIter', PROJ_MAX_ITER, ...
    'maxEval', PROJ_MAX_EVAL, 'verbose', PROJ_VERBOSE, ...
    'maxRT', PROJ_MAX_RT, 'aFeasTol', A_FEAS_TOL);
projOpts.backtracking = true;

%% Save run statistics in this struct
data = struct;
data.FACTOR = FACTOR;
data.recOpts = recOpts;
data.projOpts = projOpts;
data.solvers = 'CflashSolver and BcflashSolver';
data.infoHeader = {'proj cgTol', 'pgNorm', 'solveTime', 'fx', ...
    'nrmViol', 'nObjFunc', 'nGrad', 'nHess', 'iter', 'nProj', 'iStop'};
data.cgTols = 10.^(-1:-2:-10);
mL = length(data.cgTols);
mM = length(data.infoHeader);
data.pMat = nan(mL, mM);

[~, filename] = fetchNames(FACTOR, 'cflash-bcflash-study-cg-tol', ...
    './cflash-study/');

%% Solving everything
% Calling the models
import model.ProjModel;
import model.ProjRecModel;
import solvers.CflashSolver;
import solvers.BcflashSolvercl;

% For reconstruction solvers
nL = 1;
for cgTol = data.cgTols
    
    fprintf('\n\n --- cgTol = %d --- \n\n', cgTol);
    
    projOpts.cgTol = cgTol;
    
    % Building the projection model & solver
    projModel = ProjModel(prec, crit.J{2}.GeoS);
    projSolver = solvers.BcflashSolver(projModel, projOpts);
    
    % Building the reconstruction model & solver
    recModel = ProjRecModel(crit, prec, sino, crit.J{2}.GeoS, ...
        projSolver);
    recSolver = solvers.CflashSolver(recModel, recOpts);
    % Solving the problem
    recSolver.solve();
    
    %% Save run statistics
    srie = genSerie(recSolver.x, crit.J{2}.GeoS, sino, ...
        prec, false, '');
    nrmViol = norm(srie.Val(srie.Val < 0));
    stats = [cgTol, recSolver.pgNorm, recSolver.solveTime, ...
        recSolver.fx, nrmViol, recSolver.nObjFunc, recSolver.nGrad, ...
        recSolver.nHess, recSolver.iter, recSolver.nProj, ...
        recSolver.iStop];
    data.pMat(nL, :) = stats;
    
    % Saving data struct -- preemptively in case MATLAB crashes
    save(filename, 'data');
    
    nL = nL + 1;
end