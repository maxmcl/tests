%% IPOPT test file using AMPL problems
clc;
clear all;
close all;

%% Initializing the directories
global ROOTDIR
% Defining the root directory
ROOTDIR = fullfile(getenv('HOME'), 'Masters');
% setPaths is in recUtils
addpath(fullfile(ROOTDIR, 'recUtils'));
% Adding all the other repositories to the path
setPaths; % edit this function accordingly

%% Solve the problems
import solvers.BcflashSolver2;
import model.AmplModel;

% Set up the solvers' general parameters
solverOpts = struct('aOptTol', 1e-8, 'aFeasTol', 1e-30, ...
    'maxIter', 1e4, 'verbose', 2, 'maxRT', 2*60, 'maxEval', 1e4, ...
    'aCompTol', 1e-10, 'backtracking', true);

problem = {'/home/max/Masters/decoded_ampl_models/cute/mdhole.nl'};
% For each problem
% Load the problem, sparse = true
nlp = model.AmplModel(problem{1}, true);
fprintf('\n\n--- %s ---\n\n', problem{1});

% Calling the solver
bcflashSolver = solvers.BcflashSolver2(nlp, solverOpts);
bcflashSolver.solve();

%%
% Creating grid
% LIMITS = [-8, 10]; % hsmod3.nl
LIMITS = [-12, 12]; % mdhole.nl
STEPS = 5e2;

min_x = LIMITS(1);
max_x = LIMITS(2);
vals = linspace(min_x, max_x, STEPS);
[X, Y] = meshgrid(vals);
Y = Y(end:-1:1, :);

% Building array of objective function values
jj = 1;
Z = NaN(size(X));
for temp_x = vals
    ii = 1;
    for temp_y = vals(end:-1:1)
        Z(ii, jj) = nlp.fobj([temp_x; temp_y]);
        ii = ii + 1;
    end
    jj = jj + 1;
end

%% Plotting
fs_min_x = min_x;
fs_min_x(~isinf(nlp.bL(1))) = nlp.bL(1);
fs_max_x = max_x;
fs_max_x(~isinf(nlp.bU(1))) = nlp.bU(1);
fs_min_y = min_x;
fs_min_y(~isinf(nlp.bL(2))) = nlp.bL(2);
fs_max_y = max_x;
fs_max_y(~isinf(nlp.bU(2))) = nlp.bU(2);

h = figure;
for temp = bcflashSolver.iterLog
    % Extracting from temp
    msg = temp{1}{1};
    x = temp{1}{2};
    s = temp{1}{3};
    r = temp{1}{4};
    
    % Objective function values
%     LVLS = [2e2, 1e2, 50, 25, 10, 5, 0]; % hs3mod.nl
    LVLS = [1e4, 5e3, 2.5e3, 5e2, 100, 30, 0];
    [cc, hh] = contour(X, Y, Z, LVLS, 'LineWidth', 1.5);
    clabel(cc, hh);
    hold on;
    % Trust-region radius
    D = (X - x(1)).^2 + (Y - x(2)).^2 - r^2;
    contour(X, Y, D, [0, 0], 'r--', 'LineWidth', 1.5);
    
    % Current iterate
    if ~strcmp(msg, 'Solution')
        plot(x(1), x(2), 'b.', 'MarkerSize', 20);
    else
        plot(x(1), x(2), 'm*', 'MarkerSize', 15);
    end
    % Proposed step
    quiver(x(1), x(2), s(1), s(2), 1, 'k-', 'LineWidth', 1.5);
    
    % Add step message
    text(min_x + 1, max_x - 1, msg, 'FontSize', 12)
    
    % Add feasible set
    
    re = patch([fs_min_x, fs_max_x, fs_max_x, fs_min_x], ...
        [fs_min_y, fs_min_y, fs_max_y, fs_max_y], [0, 1, 0]);
    re.LineStyle = 'none';
    alpha(re, 0.15);
    
    hold off;
    axis([min_x, max_x, min_x, max_x]);
    pause;
end