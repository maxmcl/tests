clc;
clear all;
close all;

%% Seting MATLAB's path
addpath('~/Masters/nlplab');
addpath('~/Masters/logging4matlab/');
addpath('~/Masters/Spot');

%% Format
HEADER = {'Solver', '#iter', '#f', '#g', '#H', '|Pg|', ...
    '|x*-x|', 'RT'};
HEADER_FORMAT = ['%25s', repmat('%10s ', 1, 7), '\n'];
BODY_FORMAT = '%25s %10d %10d %10d %10d %10.1e %10.1e %10.1e\n';
outInfo = {};

%% Building the model
% Quadratic objective function, upper and lower bounds
m = 15;
n = m;
Q = diag(50 * ones(m - 1, 1), -1) + diag(100 * ones(m, 1), 0) + ...
    diag(50 * ones(m - 1, 1), 1);
c = 10 * ones(n, 1);
bL = [-inf; ones(14, 1)];
bU = [10 * ones(14, 1); inf];
x0 = zeros(n, 1);
cL = -inf(n, 1);
cU = inf(n, 1);
A = [];

%% MATLAB's quadprog is the reference solution
xRef = quadprog(Q, c, [], [], [], [], bL, bU);

%% Solve using bcflash
import model.BoundProjQpModel;
import solvers.BcflashSolver;
import solvers.PnbSolver;
import solvers.Tmp2Solver;

quadModel = BoundProjQpModel(Q, c, A, cL, cU, bL, bU, x0, '');
solver = BcflashSolver(quadModel, 'aOptTol', 1e-10, 'aFeasTol', 1e-15, ...
     'maxIter', 1e4, 'verbose', 2, 'useBb', false, 'backtracking', ...
     false, 'maxRT', 2*60);
solver = solver.solve();

quadModel = BoundProjQpModel(Q, c, A, cL, cU, bL, bU, x0, '');
solver = PnbSolver(quadModel, 'aOptTol', 1e-10, 'aFeasTol', 1e-15, ...
     'maxIter', 1e4, 'verbose', 2, 'maxRT', 2*60);
solver = solver.solve();

quadModel = BoundProjQpModel(Q, c, A, cL, cU, bL, bU, x0, '');
solver = PnbSolver(quadModel, 'aOptTol', 1e-10, 'aFeasTol', 1e-15, ...
     'maxIter', 1e4, 'verbose', 2, 'maxRT', 2*60);
solver = solver.solve();

