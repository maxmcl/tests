%% Testing MinConf_TMP to solve the projection sub-problem
clc;
clear all;
close all;

%% Initializing the directories
global ROOTDIR
% Defining the root directory
ROOTDIR = fullfile(getenv('HOME'), 'Masters');
% setPaths is in recUtils
addpath(fullfile(ROOTDIR, 'recUtils'));
% Adding all the other repositories to the path
setPaths; % edit this function accordingly
% Folder where the phantom is saved
DATADIR = fullfile(ROOTDIR, 'TestData');
% Folder where the projection matrices are saved
MPDIR = fullfile(DATADIR, 'projMatrices');
    
%% Creating the phantom data and the sinogram
FACTOR = 4;
critPars.lambda = 0.1;
critPars.delta = 5e-03;
critPars.penalType = 'PenalObj_L2';
critPars.coordType = 'cart';
% Chosing set of parameters to use for the phantom
[fant, sino] = genData(DATADIR, FACTOR, ['phant_', num2str(FACTOR)]);

% Creating the cartesian coordinates objective function object (Critere)
[crit, critPars] = genCrit(fant, sino, DATADIR, MPDIR, critPars);


%% Checking for the eigenvalues

n = crit.J{2}.GeoS.nVoxels;
wrapPtP = @(x) crit.J{1}.MPrj.RProject(crit.J{1}.MPrj.Project(x));

% Computing the eigen values using a Krylov method (provided by eigs)
opts.isreal = true;
opts.tol = 1e-10;
opts.maxit = 3 * n;
opts.issym = true;
opts.disp = 2;

% Ask for n - 3 eigen values in order for MATLAB to call eigs...
[eigsPtP, ~, flag] = eigs(wrapPtP, n, n - 3, 'BE', opts);

figure;
semilogy(eigsPtP, 'b.');
cnd = max(eigsPtP)/min(eigsPtP);
ylabel(['\lambda_{P''P} ,  max(\lambda)/min(\lambda) =', ...
    sprintf('%4.1e', cnd)]);
axis tight;
a = gca();
% a.YLim = [1e-5, 1e0];
